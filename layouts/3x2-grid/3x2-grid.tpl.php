<div class="layout-3x2-grid container-3x2-grid">

  <div class="layout-3x2-grid top-3x2-grid row-3x2-grid">

    <div class="layout-3x2-grid left-3x2-grid">
  		<?php echo $content['top-left']; ?>
  	</div>

  	<div class="layout-3x2-grid center-3x2-grid">
  		<?php echo $content['top-center']; ?>
  	</div>

  	<div class="layout-3x2-grid right-3x2-grid">
  		<?php echo $content['top-right']; ?>
  	</div>

  </div>

  <div class="layout-3x2-grid bottom-3x2-grid row-3x2-grid">

    <div class="layout-3x2-grid left-3x2-grid">
  		<?php echo $content['bottom-left']; ?>
  	</div>

  	<div class="layout-3x2-grid center-3x2-grid">
  		<?php echo $content['bottom-center']; ?>
  	</div>

  	<div class="layout-3x2-grid right-3x2-grid">
  		<?php echo $content['bottom-right']; ?>
  	</div>

  </div>

</div>
