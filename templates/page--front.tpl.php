<?php
/**
 * @file
 * Zen theme's implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region, below the main menu (if any).
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 * - $page['bottom']: Items to appear at the bottom of the page below the footer.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess_page()
 * @see template_process()
 */
?>
<div class="page">
<!-- The call to action is manually turned on by removing this comment.
  <div class="call-to-action">
    <div class="call-to-action-text"><h1><span>>> </span><a href="apply">Apply by Jan. 31</a><span> <<</span></h1></div>
</div> -->
  <header role="banner">

  <!-- START WESTERN HEADER -->

    <section class="western-header" aria-label="University Links, Search, and Navigation">
      <div class="center-content">
        <span class="western-logo"><a href="http://www.wwu.edu">Western Washington University</a></span>
        <?php if ($site_name): ?>
        <span class="site-name"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></span>
        <?php endif; ?>
        <nav role="navigation" aria-label="WWU menu">
          <div class="western-quick-links" aria-label="Western Quick Links">
            <button role="button" aria-pressed="false" aria-label="Toggle Quick Links">Toggle Quick Links</button>
            <ul>
              <li><a href="http://www.wwu.edu/academic_calendar" title="Calendar"><span aria-hidden="true">c</span> <span>Calendar</span></a></li>
              <li><a href="http://www.wwu.edu/directory" title="Directory"><span aria-hidden="true">d</span> <span>Directory</span></a></li>
              <li><a href="http://www.wwu.edu/index" title="Index"><span aria-hidden="true">i</span> <span>Index</span></a></li>
              <li><a href="http://www.wwu.edu/campusmaps" title="Map"><span aria-hidden="true">l</span> <span>Map</span></a></li>
              <li><a href="http://mywestern.wwu.edu" title="myWestern"><span aria-hidden="true">w</span> <span>myWestern</span></a></li>
            </ul>
          </div>
          <div class="western-search" role="search" aria-label="University and Site Search">
            <button role="button" aria-pressed="false" aria-label="Open the search box">Open Search</button>
            <div class="western-search-widget">
              <?php print $search_box; ?>
            </div>
          </div>
          <button class="mobile-main-nav" role="button" aria-pressed="false" aria-label="Open Mobile Main Navigation">Open Main Navigation</button>
        </nav>
      </div>
    </section>

    <!-- END WESTERN HEADER -->

    <!-- START SITE HEADER -->

    <section class="site-header" aria-label="Site Header">

      <!-- START VIDEO -->

      <div class="video-container">
        <div class="video-fluid-wrapper">
<iframe src="//player.vimeo.com/video/103260141?title=0&byline=0&portrait=0&autoplay=1&loop=1" width="1280" height="720" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>        </div>
      </div>

      <!-- END VIDEO -->

      <!-- START SITE NAME -->
      <div class="site-name">
        <?php if ($site_name): ?>
        <p><span><?php print $site_name; ?></span></p>
        <?php endif; ?>
      </div>

      <!-- END SITE NAME -->

      <!-- START MAIN NAV -->

      <nav class="main-nav" id="main-menu" role="navigation" aria-label="Main admissions menu">
        <?php print render($page['navigation']); ?>
      </nav>

      <!-- END MAIN NAV -->

      <?php print render($page['header']); ?>
    </section>

    <!-- END SITE HEADER -->

</header>


<section class="content column">
  <?php print render($page['highlighted']); ?>
  <?php print render($title_suffix); ?>

  <?php print render($tabs); ?>
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
  <ul class="action-links"><?php print render($action_links); ?></ul>
<?php endif; ?>
<?php print render($page['content']); ?>
<?php print $feed_icons; ?>
</section>


<main>
  <!---------panel 1 ATTENTION ---------->
  <div class="panel-attention-wrapper frontpage-panel-wrapper">
    <div class="panel-attention frontpage-panel">

      <div class="header-row">

        <!-- because template is hardcoded, PHP messages (IE cache clear) moved into this section for readability)-->

         <?php print $messages; ?>
        <h2 class="panel-heading">
          <span>ACADEMIC CHOICE,</span><br>
          <span>PERSONAL ATTENTION</span>
        </h2>
      </div>

      <div class="main-row">
      <div class="left-column">

        <p class="large-number"> 160+ </p>
        <p class="no-mtop">academic programs, from the classics to those you get to design yourself</p>
      </div>

      <div class="right-column">

        <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-QuoteLtBlue.png" width="70" height="70" alt="Quote" class="quote-img">
          <p class="quote-text">Most of the classes are small. By the end of my first week at Western, all of my professors knew my name."</br>
            <span class="quote-italic italic">—Dessa Meehan</span></p>

        </div>
      </div>
    </div>
  </div>

  <!---------panel 2 VALUE ---------->
  <!--<div class="grey-lg-arrow-down"></div>-->
  <div class="panel-value-wrapper frontpage-panel-wrapper">
  <div class="panel-value">
    <div class="center-content">

      <h2 class="panel-heading">
        <span>QUALITY EDUCATION,</span><br>
        <span>GREAT VALUE</span>
      </h2>

      <div class="column-wrapper">
      <div class="left-column">
        <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-Value.png" class="img-responsive" alt="Bellingham Photos">
        <p class="white-text-row">Among the best values in the nation </p>
        <p class="blue-text-row">Kiplinger's Personal Finance and Washington Monthly magazines</p>
      </div>
      <div class="center-column">
        <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-GradCap.png" class="img-responsive" alt="Bellingham Photos">
        <p class="white-text-row">Our small class sizes give students the attention they need to succeed</p>
      </div>
      <div class="right-column">
        <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-Ribbon.png" class="img-responsive" alt="Bellingham Photos">
        <p class="white-text-row">No. 1 public, master's-granting university in the Pacific Northwest</p>
        <p class="blue-text-row">US News & World Report</p>
      </div>
    </div>



      <div class="employer-row">

        <p class="top-employer-p-style">SOME TOP EMPLOYERS OF WESTERN GRADS</p>

        <ul class="list-row clearfix">
          <li>Boeing</li>
          <li>Microsoft</li>
          <li>Amazon</li>
          <li>Google</li>
        </ul>
      </div> <!-- end employer row -->

    </div>  <!-- end center-content -->
  </div>
</div>
  <!-- end panel-value -->


  <!-- Panel 3 - location -->

  <div class="panel-location-wrapper frontpage-panel-wrapper">
  <div class="panel-location">
      <div class="location-column-left">
        <h2 class="panel-heading">
          <span>FROM THE MOUNTAINS</span><br>
          <span>TO THE BAY</span>
        </h2>
        <p class="blurb-style">80,000-person coastal city with a thriving arts and music scene <br>
6 miles of trails adjacent to campus <br>
55 miles to Vancouver, BC and 90 miles to Seattle</p>

          <img src="<?php print $base_path . $directory; ?>/images/front-page/Location-Map.png" class="img-responsive" width="200" height="185" alt="Map of Washington state">
        </div>


      <div class="location-column-right">
          <img src="<?php print $base_path . $directory; ?>/images/front-page/Location-Photos2.gif" class="img-responsive" alt="Bellingham Photos">
      </div>
    </div>
</div>

    <!-- End panel 3 -->

    <!-- Panel 4 Undergrads -->

    <div class="panel-undergrads-wrapper frontpage-panel-wrapper">
    <div class="panel-undergrads">
        <div class="undergrad-column-left">
          <h2 class="panel-heading">
            <span>AT WESTERN, OUR FOCUS IS ON</span><br>
            <span>UNDERGRADUATES</span>
          </h2>
          <p>Western undergraduates work directly with professors
            on research that will change lives.</p>
            <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-QuoteDkBlue.png" class="img-responsive" alt="Quotes">
            <p class="quote-text">Studying biochemistry has allowed me to
              continue to pursue my dream of making
              positive contributions to the medical field." <br />
              <span>—Anne d’Aquino, Biochemistry</span>
            </p>

            <p><em>Anne is working with a professor on research
              that will be used as a stepping stone to ultimately
              alleviate the medical consequences of hemophilia A.</em></p>
            </div>
            <div class="undergrad-column-right">
              <img src="<?php print $base_path . $directory; ?>/images/front-page/StudentProfile.jpg" class="img-responsive" alt="Student picture">
            </div>

        </div>
</div>
        <!-- end panel 4 -->

        <!-- panel 5 students -->

        <div class="panel-students-wrapper frontpage-panel-wrapper">
        <div class="panel-students">
          <div class="center-content">
            <div class="top-row">
              <h2 class="panel-heading">
                <span>OUR STUDENTS STRIVE</span><br>
                <span>TO MAKE A DIFFERENCE IN THE WORLD</span>
              </h2>
            </div>

            <div class="left-column">
              <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-PeaceCorps.png" class="img-responsive">
              <p>More Western graduates join the Peace Corps than any other school of its size</p>
            </div>

            <div class="center-column">
              <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-Bulb.png" class="img-responsive">
              <p>First school in the nation to offset 100% of electrical usage with renewable energy thanks to student initiatives </p>

            </div>

            <div class="right-column">
              <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-Building.png" class="img-responsive">
              <p>An active student association supports 200+ campus clubs and organizations</p>

            </div>

            <div class="row2">
              <div class="slogan">
                <p>ACTIVE MINDS CHANGING LIVES</p>
              </div>
            </div>

            <div class="row3">
              <div class="fulbright">
                <p>
                  Fulbright Awards are a prestigious measure of student success. Western has more Fulbright winners than any peer school in the nation.
                </p>
              </div>

            </div>
          </div>
        </div>
</div>
          <!-- end panel 5 -->

          <!-- panel 6 -->


        <div class="panel-get-started-wrapper frontpage-panel-wrapper clearfix">
          <div class="panel-get-started">

            <div class="header-text">
              <h2 class="panel-heading">GET STARTED</h2>
            </div>
           <div class="top-row">
              <a id="apply-now" class="custom-buttons" href="apply-now" onClick="ga('send', 'event', 'FunnelLinks', 'clickedApplyNow', 'Apply Now Link');">APPLY NOW</a>


              <a id="freshman-button" class="custom-buttons" href="freshman" onClick="ga('send', 'event', 'FunnelLinks', 'clickedFreshman', 'Freshman and Running Start Link');">Freshman & Running Start</a>


              <a id="transfer-button" class="custom-buttons" href="transfer" onClick="ga('send', 'event', 'FunnelLinks', 'clickedTransfer', 'Transfer Link');">Transfer</a>


              <a id = "international-button" class="custom-buttons" href="international" onClick="ga('send', 'event', 'FunnelLinks', 'clickedInternational', 'International Link');">International</a>


            </div>

           <div class="bottom-row">
              <a id="returning-button" class="custom-buttons" href="returning-student" onClick="ga('send', 'event', 'FunnelLinks', 'clickedReturning', 'Returning Link');">Returning</a>
              <a id="post-bac-button" class="custom-buttons" href="post-bac" onClick="ga('send', 'event', 'FunnelLinks', 'clickedPostBaccalaureate', 'Post-baccalaureate Link');">Post-baccalaureate</a>
              <a id="extended-button" class="custom-buttons" href="extended-ed" onClick="ga('send', 'event', 'FunnelLinks', 'clickedExtendedEducation', 'Extended Education Link');">Extended Education</a>
              <a id="graduate-button" class="custom-buttons" onClick="ga('send', 'event', 'FunnelLinks', 'clickedGraduateStudent', 'Graduate Student Link');">Graduate Student</a>
           </div>

        </div>
</div>
          <script type="text/javascript">
          //have to hard code button links - relative to root except the last one.

             document.getElementById("graduate-button").onclick = function () {
                location.href = "http://www.wwu.edu/gradschool/";
            };
          </script>


        <!-- end panel 6 -->

        <!-- end grid wrapper -->
</main>


    <?php if ($secondary_menu ): ?>
    <nav class="secondary-nav" role="navigation" aria-role="Secondary navigation">
      <?php print theme('links__system_secondary_menu', array(
        'links' => $secondary_menu,
        'attributes' => array(
          'class' => array('links', 'inline', 'clearfix'),
          ),
        'heading' => array(
          'text' => $secondary_menu_heading,
          'level' => 'h2',
          'class' => array('element-invisible'),
          ),
          )); ?>
        </nav>
      <?php endif; ?>

      <?php
      $sidebar_first  = render($page['sidebar_first']);
      $sidebar_second = render($page['sidebar_second']);
      ?>

      <?php if ($sidebar_first || $sidebar_second): ?>
      <aside class="content-sidebar">
        <?php print $sidebar_first; ?>
        <?php print $sidebar_second; ?>
      </aside>
    <?php endif; ?>

</div>
<!-- end div.page -->

<footer role="contentinfo">
  <div class="footer-wrapper">

    <div class="footer-left">
      <?php print render($page['footer_left']); ?>
    </div>

    <div class="footer-center">
      <?php print render($page['footer_center']); ?>
      <div class="western-privacy-statement">
        <a href="http://www.wwu.edu/privacy/">Website Privacy Statement</a>
      </div>
    </div>

    <div class="footer-right" role="complementary">
      <h1><a href="http://www.wwu.edu">Western Washington University</a></h1>

      <div class="western-contact-info">
        <p class="western-address">516 High Street<br>
          Bellingham, WA 98225</p>
          <p class="western-telephone"><a href="tel:3606503000">(360) 650-3000</a></p>
          <p class="western-contact"><a href="http://www.wwu.edu/wwucontact/">Contact Western</a></p>
        </div>

        <div class="western-social-media">
          <ul>
            <li><a class="westernIcons-FacebookIcon" href="http://www.facebook.com/westernwashingtonuniversity">Facebook</a></li>
            <li><a class="westernIcons-FlickrIcon" href="http://www.flickr.com/wwu">Flickr</a></li>
            <li><a class="westernIcons-TwitterIcon" href="https://twitter.com/#!/WWU">Twitter</a></li>
            <li><a class="westernIcons-YouTubeIcon" href="http://www.youtube.com/wwu">Youtube</a></li>
            <li><a class="westernIcons-GooglePlusIcon" href="https://plus.google.com/+wwu/posts">Google Plus</a></li>
            <li><a class="westernIcons-RSSicon" href="http://news.wwu.edu/go/feed/1538/ru/atom/">RSS</a></li>
          </ul>
        </div>


        <?php print render($page['footer_right']); ?>
      </div>
    </div> <!-- end div.footer-wrapper -->
  </footer>

  <?php print render($page['bottom']); ?>
