meep
#WWU Admissions Theme WWUZEN-Admissions#
A WWUZEN Zen Sub-Theme.
Its. Test. Stuff.
Foo.
##Methodology##
The reason to have a sub-theme per college/department is to allow for further customization of the site than is available out of the box with the stock WWUZEN theme. Utilizing a separate sub-theme allows for us to make updates to WWUZEN that a site can inherit without fear of clobbering how the site currently works. The only file currently needed to edit is sass/customization.scss. Please only include CSS in here that is specific to the Admissions site, such as blocks that are only for Admissions, or Panel content.

##Dependencies##
  * [WWUZEN] git clone git@bitbucket.org:wwuweb/wwuzen.git

##Configuration##
1. Read the WWUZEN Readme (https://bitbucket.org/wwuweb/wwuzen) and make sure all of those dependecies are in place.



###Copyright Statement###

Copyright 2013 Western Washington University Web Technologies Group Licensed under the Educational Community License, Version 2.0 (the "License") you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.osedu.org/licenses/ECL-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied. See the License for the specific language governing permissions and limitations under the License.
