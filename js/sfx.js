// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.siteNameTypography = {
    attach: function () {

      // Gets copy from site name (ultimately inside a span: #site-name a span)
      var siteName = $("#site-name").text();

      var typographyAnd = siteName.replace("and", "<span class=\"diminutive-type site-name-and\">and</span>");
      var typographyCollegeOf = typographyAnd.replace("of the", "<span class=\"diminutive-type site-name-college-of\">of the</span>");

      $('#site-name a span').replaceWith(typographyCollegeOf);
    }
  }

  /*
     Description:
     This function allows the Calendar on the Admissions website to output links that are properly formatted to work with the
     CGI script over at applyweb. It requres Content Types set up with the Tour Type as a required taxonomy term,
     and for the field output to be rewritten as [field_tour_type] with class 'rewrite-link.'

     Dependencies:
     This function depends on the excellent moment.js library to handle formatting and output - more info about
     moment.js at http://momentjs.com/.

     Please contact bryce.danz@wwu.edu or brycedanz __at__ gmail.com for support.

*/

  Drupal.behaviors.calendarLinks = {
    attach: function () {
      var items;
      //process each tour item
      $('.rewrite-link').each( function (item) {

        //get tour date from ancestor td id DOM element
        var tourDate = $(this).closest('td').attr('id');

        /* clean up and parse the PHP output - this is currently set up for output of form
           [field_tour_type]. We can't pull the tour type from the DOM because it
           is contained within a "title" attribute that has not been set yet when this script fires (although if we really wanted
           to get creative we could inject it into the DOM with tokens.)
           */
        stringToParse = $(this).text();
        stringToParse = stringToParse.trim();
        stringToParse = stringToParse.split(' ');

        /* filters out whitespace-only elements from the parse input. Super helpful here because Drupal puts in a million Divs and we only want ones
           with relevant information. Please see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions for more information*/
        stringToParse = stringToParse.filter(function(str) {
          return /\S/.test(str);
        });

        tourDate = tourDate.split('-');
        //get raw date info for formatting into a Moment object for output into the link
        var rawDay =tourDate[3];
        var rawMonth = tourDate[2];
        var rawYear = tourDate[1];

        //create moment.js object
        visitDate = moment(rawYear + rawDay + rawMonth, "YYYYDDMM");

        //format with moment.js
        dateParam1Raw = visitDate.format('YYYY[-]MM[-]DD');
        dateParam2Raw = visitDate.format('dddd[+]MMMM[+]Do[,]+YYYY');

        //handle day of the week
        var visitType = stringToParse.join(' ').trim();
        var visitTypeDisplay = visitType;

        //holds node path for WFW, SIW, WP
        var nodeId;
        var nodeUrl;

        //rewrite tour type to conform with ApplyWeb URL parameters
        var useApplyWeb;

        switch (visitType) {
          case 'Campus Tour':
            if (visitDate.day() == 6 || visitDate.day() == 0) {
              visitType = 'Saturday+Campus+Tour';
            }
            else {
              visitType = 'Weekday+Campus+Tour';
            }
            useApplyWeb = true;
            break;
          case "Discovery Day":
            visitType = "Discovery+Days";
            useApplyWeb = true;
            break;
          case "Transfer Day":
            visitType = "Transfer+Discovery+Days";
            useApplyWeb = true;
            break;
          case "Western Fall Welcome":
            nodeId = 401;
            useApplyWeb = false;
            break;
          case "Western Preview":
            nodeId = 396;
            useApplyWeb = false;
            break;
          case "Spring Into Western":
            nodeId = 402;
            useApplyWeb = false;
            break;

          default:
            useApplyWeb = false;
        };

        //only adjust the url if we have applyweb logic to send it to
        if (useApplyWeb == true) {
          //generate ApplyWeb URL
          baseUrl = 'https://www.applyweb.com/cgi-bin/register?s=wwuvisit';
          visitTypeParam = 'WWU_VISIT_REG_TYPE=' + visitType;
          dateParam1 = 'WWU_VISIT_REG_DATE_TIME=' + dateParam1Raw;
          dateParam2 = 'WWU_VISIT_REG_DATE_TIME_DISPLAY=' + dateParam2Raw;
          applyWebUrl = baseUrl + '&' + visitTypeParam + '&' + dateParam1 + '&' + dateParam2;

          //replace PHP output with properly formatted links
          var targetUrl = '<span><a href=\'' + applyWebUrl + '\'>' + visitTypeDisplay + '</a></span>';
        }

        /*
           This case is for the Western Fall Welcome, Western Preview and Spring into Western links. Routing logic
           for these can be implemented here.

*/
        else {
          nodeUrl = 'http://admissions.wwu.edu/node/' + nodeId;
          targetUrl = '<span><a href=\'' + nodeUrl + '\'>' + visitTypeDisplay + '</a></span>';
        }

        $(this).replaceWith(targetUrl);
      });
    }
  }


  Drupal.behaviors.psatFormAutoSumbit = {
    attach: function (context, settings) {
      //Check that the form is fully filled out, put an error in the console if the form is not filled out entirely
      //We want this to only run on the PSAT Form so we target the ID of the form
      var completedField = true;
      $('form#webform-client-form-159 .required').each(function () {
        if ($(this).val() == "") {
          completedField = false;
          console.log("The field " + $(this).attr('id') + " is not filled out.");
          //If it returns that means the field is not filled out so stop executing the script
        }});

      //If the form is filled out properly, submit it once the DOM loads
      if (completedField == true) {
        $(document).ready(function () {
          $("#webform-client-form-159").submit();
        })}

    }};

  Drupal.behaviors.showMainMenu = {
    attach: function (context, settings) {
      var $mainMenu;
      var $mainMenuParent;
      var $mainMenuContainer;
      var $menuItems;
      var opened;
      var $submenuItems;
      var timeout;
      var $window;

      function bindHandlers() {
        $menuItems.unbind('click', clickMenuItem);
        $submenuItems.unbind('keyup', keyupMenuItem);

        if ($window.width() > 800) {
          $menuItems.click(clickMenuItem);
          $submenuItems.keyup(keyupMenuItem);
        }
      }

      function resetMainMenu() {
        if ($window.width() <= 800 && opened !== null) {
          hideMenuContainer();
          $(opened).removeClass('opened');
          $(opened).children('ul').hide();
          opened = null;
        }
      }

      function showMenuContainer() {
        $mainMenuContainer.animate({
          'height': '175px'
        }, timeout);

        $mainMenuParent.animate({
          'height': '168px'
        }, timeout);
      }

      function hideMenuContainer() {
        $mainMenuContainer.animate({
          'height': '71px'
        }, {
          'duration': timeout,
          'complete': function () {
            $mainMenuContainer.css('height', 'auto');
          }
        });

        $mainMenuParent.animate({
          'height': '64px'
        }, {
          'duration': timeout,
          'complete': function () {
            $mainMenuParent.css('height', 'auto');
          }
        });
      }

      function keyupMenuItem(event) {
        var $this;

        $this = $(this);

        switch (event.which) {
          case 27: // ESCAPE
            $this.parents('.opened').removeClass('opened');
            hideMenuContainer();
            $this.closest('ul').slideUp(timeout);
            opened = null;
        }
      }

      function clickMenuItem(event) {
        var $currSubmenu;
        var $prevSubmenu;
        var $opened;
        var $this;

        $this = $(this);
        $currSubmenu = $this.children('ul');

        if (!$currSubmenu.length) return;

        $menuItems.unbind('click', clickMenuItem);

        // Unbind the click handler for the duration of menu animation
        setTimeout(function () {
          $menuItems.click(clickMenuItem);
        }, timeout);

        if (opened === this) {
          $this.removeClass('opened');
          hideMenuContainer();
          $currSubmenu.slideUp(timeout);
          opened = null;
        } else if (opened === null) {
          $this.addClass('opened');
          showMenuContainer();
          $currSubmenu.slideDown(timeout);
          opened = this;
        } else {
          $this.addClass('opened');
          $opened = $(opened);
          $opened.removeClass('opened');
          $prevSubmenu = $opened.children('ul');
          $prevSubmenu.fadeOut(timeout);
          $currSubmenu.fadeIn(timeout);
          opened = this;
        }
      }

      opened = null;
      timeout = 300;
      $window = $(window, context);
      $mainMenuContainer = $('#main-menu', context);
      $mainMenuParent = $('#main-menu .menu-name-main-menu', context);
      $mainMenu = $('#main-menu .menu-name-main-menu > .menu', context);
      $menuItems = $mainMenu.children('li');
      $submenuItems = $menuItems.find('li');

      // EVENT HANDLERS

      // Allow default on the link to the home page by slicing the first element
      // from the selection
      $menuItems.children('a').slice(1).click(function (event) {
        event.preventDefault();
      });

      $window.resize(function() {
        resetMainMenu();
        bindHandlers();
      });

      bindHandlers();
    }
  };

  /**
   * Reorder menu and content for mobile on admissions site. Uses debounce to avoid performance hit
   * Calls immediately, and then every .75 seconds on page resize.
   * Questions? Brycedanz@gmail.com
   *
   */

  Drupal.behaviors.mobileOrdering = {
    attach: function (context, settings) {

      //debounce from the wonderful Underscore.JS
      function debounce(func, wait, immediate) {
        var timeout;
        return function() {
          var context = this
            args = arguments;
          clearTimeout(timeout);
          timeout = setTimeout( function()  {
            timeout = null;
            if (!immediate) func.apply(context, args);
          }, wait);
          if (immediate && !timeout) func.apply(context, args);
        };
      };

      var menu = $('.wwu-25-percent-left-column');
      var content = $('.wwu-75-percent-right-column');

      //calls function with debounce wrapper to smooth performance
      //@param 50 is for .05 seconds per function call, should be performant without causing flashing
      var rearrangeContentForMobile = debounce( function() {
        if ($('.site-header').css('position') == "static" ) {
          $(content).insertBefore(menu);
        }
      }, 50);


      /*
         Using .site-header static vs relative property to catch breakpoint
         See: http://www.fourfront.us/blog/jquery-window-width-and-media-queries
         */
      function rearrangeContentForMobileInit() {
        if ($('.site-header').css('position') == "static" ) {
          $(content).insertBefore(menu);
        }
      }

      //fire on page load
      rearrangeContentForMobileInit();
      //fire on window resize
      window.addEventListener('resize', rearrangeContentForMobile);

    }
  };

  Drupal.behaviors.calendarHeaderFix = {
    attach: function (context, settings) {
      var dateToRewrite = $('.view-header .date-heading H3').text();
      var dateMomentObject = moment(dateToRewrite);
      $('.view-header .date-heading H3').text(dateMomentObject.format('MMMM YYYY'));
    }
  };


  Drupal.behaviors.hideNextButtonCalendar = {
    attach: function (context, settings) {

      var currentMonthRaw = moment();
      var displayMonth = moment();
      var currentMonthFetched = $('.date-heading').text().trim();
      var currentDateTestString = currentMonthRaw.format("MMMM YYYY");

      if (currentDateTestString === currentMonthFetched) {
        $('.date-prev').css("display", "none");
      }

      /*
         We want to hide the calendar's next button if it is more than 6 months in advance, so
         we add 5 months to the current date.
         */

      currentMonthRaw.add('months', 5).calendar();
      currentDateTestString = currentMonthRaw.format("MMMM YYYY");

      if (currentDateTestString === currentMonthFetched) {
        $('.date-next').css("display", "none");
      }
    }
  };

  /*
     Sets all background colors to the stripe color and sets some text to white depending on readabililty
     */


  Drupal.behaviors.colorCalendar = {
    attach: function (context, settings) {

      $('.stripe').each( function () {

        var color = $(this).css('background-color');
        $(this).parent().css('background-color', color);

        if (color == 'rgb(51, 101, 159)' || color == 'rgb(51, 137, 101)' || color == 'rgb(214, 87, 89)') {
          $(this).parent().find('a').css('color', '#fff');
        }
      });
    }
  };

  /*
     Javascript to make the Menu on the front page sticky on Desktop-sized viewports.

     Todo in wave 2:
     1.) Debounce functions for resize and scroll to improve performance

*/


  Drupal.behaviors.stickyMenu = {
    attach: function (context, settings) {
      var main_menu,
      main_menu_container,
      offset_top,
      $window;

      function sticky_navigation() {
        var cur_scroll_pos,
            offset_top;

        // get the current position of the top of the viewport on the page
        cur_scroll_pos = $window.scrollTop();

        // get the vertical offset of the main menu container
        offset_top = main_menu_container.offset().top;

        // case: nav menu is higher on page than viewport and viewport is
        // desktop sized - make menu sticky with fixed positioning
        // using maximum fixed width for menu to avoid it stretching off the
        // viewport and looking unattractive.
        if (cur_scroll_pos > offset_top) {
          main_menu_container.addClass('fixed-menu-active');
          main_menu.addClass('fixed-menu-active');
        }

        // if the user is in the top part of the page looking at the video or
        // not on a desktop, use default menu styles
        else {
          main_menu_container.removeClass('fixed-menu-active');
          main_menu.removeClass('fixed-menu-active');
        }
      }

      // Only needs to run on the front page
      if ($('body').hasClass('front')) {
        $window = $(window);
        main_menu_container = $('#main-menu');
        main_menu = main_menu_container.children('.region-navigation');

        // The function should fire every time the user loads the page, scrolls
        // up or down, or resizes the page to mobile (this is unlikely to happen
        // much in the wild, though)
        sticky_navigation();
        $window
          .scroll(sticky_navigation)
          .resize(sticky_navigation);
      }
    }
  };

  Drupal.behaviors.frontPageVideoResize = {

    /**
     * Debouncing technique: http://www.html5rocks.com/en/tutorials/speed/animations/
     */
    attach: function (context, settings) {
      var $videoContainer,
      $window;

      function getMaxHeight(currentWindowHeight) {
        var height;

        height = currentWindowHeight - 120;

        return (height < 400) ? 400 : height;
      }

      function resize() {
        $videoContainer.css('max-height', getMaxHeight($window.height()) + 'px');
      }

      $videoContainer = $('.video-container');
      $window = $(window);
      $window.resize(resize);
      resize();
    }
  };

})(jQuery, Drupal, this, this.document);
