  // From the customize.js file
  /**
   * Holds custom JavaScript for the Quick-facts view in the wwu admissions site.
   *
   */
  (function () {
    "use strict";
    var category = document.getElementsByClassName("view-grouping-header");
    var counter = category.length;
  
    //Loop through category containers, find category names and add the correct icon image class
    if(counter !== 0) {
      for(var i = 0; i < counter; i++) {
        switch(category[i].getElementsByTagName("a")[0].innerHTML.trim()) {
          case "Academics":
            category[i].className += " category-academics";
            break;
          case "Location":
            category[i].className += " category-location";
            break;
          case "Distribution of Undergraduate Degrees":
            category[i].className += " category-degrees";
            break;
          case "Student Life":
            category[i].className += " category-life";
            break;
          case "Retention and Graduate Rates":
            category[i].className += " category-degrees";
            break;
          default:
            category[i].className += " category-enrollment";
            break;
        }
      }
    }

  })();